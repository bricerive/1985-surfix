![Tape front cover](https://bitbucket.org/bricerive/1985-surfix/raw/HEAD/images/Front.jpg)

# Surfix
This is a project developed by Brice Riv�.

## Overview
Surfix uses a pseudo hiRes routine to generate hiRes graphics on a plain-vanilla ZX81 with 16Ko extension.
I coded Surfix fully in Z80 assembly

## Demo videos
[![Alt text](https://img.youtube.com/vi/x-5aq-VSyGQ/0.jpg)](https://www.youtube.com/watch?v=x-5aq-VSyGQ)
[![Alt text](https://img.youtube.com/vi/yP8WaVdO3EA/0.jpg)](https://www.youtube.com/watch?v=yP8WaVdO3EA)
[![Alt text](https://img.youtube.com/vi/MOkLqGflA6s/0.jpg)](https://www.youtube.com/watch?v=MOkLqGflA6s)
