################################################################
190205 review what's there
----------------------------------------------------------------
The goal was to read the program from tape and convert it into an emulator format
----------------------------------------------------------------
I did it in two steps:
130418: Recorded the tape but could not get a proper .P
130426: Cleaned up the recording and got a correct .P from the NORMAL side

----------------------------------------------------------------
Recording from a tape deck:
-> AIF or WAV

Use Audacity to view/crop the clips

Use ZX81 Tape Utilities (http://www.zx81stuff.org.uk/zx81/tapeutils/overview.html)
to convert .WAV to .p

----------------------------------------------------------------
Note that the tape had two different sides (see pictures)
Side A: NORMAL          ~ 4minutes
Side B: TURBO LOADER    ~ 45seconds

Also note that I originally swapped the names (A<->B)
----------------------------------------------------------------

ZX81 Tape Utilities lets you correct damaaged bits by hand

I beleive that's what I had to to finally load the NORMAL side.


################################################################
Note that TapeUtilities also has a .p list feature:
----------------------------------------------------------------

java -classpath tapeutils.jar tapeutils.zx81.PFileUtils -p 3DMonsterMaze.p

example

$ java -classpath tapeutils.jar tapeutils.zx81.PFileUtils -p /Volumes/Brice-SnowLeopard/MyDocuments/Emulation/ZX81/ZX81-Brice/1985-Cramp/CRAMP/CRAMP.P
Basic program:
   1 REM <76 NEWLINE><76 NEWLINE><C3>[F]RND<C3>[Y]RND<C3>SIN RND<C3>-INKEY$<C3><6D>
INKEY$  4224  [?][?][2]["]["]["][2][?]   22   5 <5A>:[S]{T}4Q[2]7(CLS QTAN 7$4NEXT
TAN Y{A}GOSUB <47><>5SQR RNDTAN Y3GOSUB <47><>5{Q}{2}TAN GOSUB <5F>{1}{1}{2}YPRINT
LN [P]{2}Y{A}GOSUB <47>GOSUB <47>5 TO  OR )5 NEXT :RETURN {T}{A}(RETURN {T}[R]GOSUB
<78 K/L>PEEK COPY ;LN <44> {5}TAB LOAD RNDLN [>]{2}LN 4{2}<>5SQR RNDY<7F cursor><=
RETURN 3S{7}LN SIN RNDY3GOSUB <47><C3>[8]{2}E<7B>RNDY{2}LN [<]INKEY$U5RNDX4{T})SAVE
RETURN ;/CX4£7<7E number>RETURN TAN 43) STEP COPY ;/;X4{T}){A}{1};/(X4£F<7E number>
RETURN TAN 4{E})4 ;/{1}INT 6<7B>RND<7E number>RETURN [2]C{S}RETURN 2{1}  4{1}{7}{7}
TAN Y LN [<]INKEY${1}  TAN {T}*GOSUB <5B>£RND55 ;FOR 5COPY <59><7,[R]CIF RETURN <76 NEWLINE>
C{5}LN [<]INKEY$/LET STR$ )SCROLL  ;SGN (DIM TAN VAL STR$ FAST FAST 5[<]RND{T}{E}
<4F>{Q}(CLEAR <4F>{D}FOR {T}{A}LPRINT ,<77 RUBOUT><STR$ )5 ;SGN (PRINT LPRINT SGN
AT TAN <C3>W<7A><76 NEWLINE>
   2 REM
   3 REM  [*][*][*][*][*][*][*][*][*][*][*][*][*][*][*][*][*][*][*][*][*][*]
     [*][ ][ ][ ][ ][ ][ ][ ][ ][C][R][A][M][P][ ][ ][ ][ ][ ][ ][ ][*]
[*][E][C][R][I][T][ ][P][A][R][ ][B][R][I][C][E][ ][R][I][V][E][*]          [*][ ]
[ ][P][O][U][R][ ][L][E][ ]ZX81[ ]16K[ ][ ][*]          [*][*][*][*][*][*][*][*][*]
[*][*][*][*][*][*][*][*][*][*][*][*][*]
   4 REM
   5 GOTO 10
   6 CLEAR
   7 SAVE "CRAM[P]"
  10 REM
  20 GOSUB 8000
  30 LET TABL=1
  40 LET VIE=3
  50 LET SCORE=0
  60 GOSUB 7000
  70 IF INKEY$="" THEN GOTO 70
  80 LET C$=INKEY$
  90 POKE 16417,PEEK 16417*(C$="")+(C$="7")+2*(C$="8")+3*(C$="6")+4*(C$="5")
 100 LET A=USR 16525
 110 LET TIME=TIME+1
 120 IF TIME=TOT THEN GOSUB 6000
 130 GOTO 80*(NOT A)+180*(A=1)+140*(A=2)
 140 LET TABL=TABL+1
 150 LET SCORE=SCORE+TIME
 160 IF TABL=13 THEN GOTO 4000
 170 GOTO 60
 180 LET VIE=VIE-1
 190 LET SCORE=SCORE+TIME
 200 IF VIE=0 THEN GOTO 5000
 210 GOTO 60
4000 CLS
4010 RAND USR 16522
4020 PRINT AT 5,10;"BRAVISSIMO"
4030 PRINT AT 10,0;"VOUS AVEZ REUSSI A SORTIR SANS  TROP DE DOMMAGES"
4040 PRINT AT 13,0;"JE VOUS PROPOSE MAINTENANT UN   AUTRE JEU:";
4050 PRINT "ESSAYEZ DE COMPRENDRE LE PROGRAMME ET DE L""AMMELIORER CAR IL EST LOI
N D""ETRE PARFAIT."
4060 PRINT "SI VOUS LE FAITES,ENVOYEZ MOI LERESULTAT A L""ADRESSE SUIVANTE"
4070 PRINT "B.RIVE 31 R.CAVENDISH 75019PARIS"
4080 PRINT "MERCI."
4090 STOP
5000 CLS
5010 RAND USR 16522
5020 PRINT AT 5,0,"VOUS AVEZ MALHEUREUSEMENTEPUISE VOS VIES."
5030 PRINT AT 21,0," PRESSEZ ""R""POUR REJOUER"
5040 IF INKEY$="" THEN GOTO 5040
5050 IF INKEY$="R" THEN RUN 30
5060 STOP
6000 LET P=23040+33*8*INT (RND*19)+INT (RND*32)
6010 IF PEEK P<>158 THEN GOTO 6000
6020 POKE P,30
6030 POKE P+33,30
6040 POKE P+66,160
6050 POKE P+99,160
6060 POKE P+132,160
6070 POKE P+165,160
6080 POKE P+198,30
6090 POKE P+231,30
6100 RETURN
7000 LET START=25685
7010 POKE 16507,START-256*INT (START/256)
7020 POKE 16508,INT (START/256)
7030 LET TOT=80+80*(TABL>6)
7040 LET TIME=0
7050 POKE 16417,2
7060 RAND USR 16516
7070 RAND USR 16519
7080 CLS
7090 PRINT "SCORE:";SCORE,"VIES:";VIE
7100 PRINT "{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}
{1}{1}{1}{1}{1}{1}{1}{1}{1}"
7110 PRINT AT 21,0;"{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}
{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}"
7120 GOSUB 7500+50*(TABL-6*(TABL>6))
7130 RAND USR 16528
7140 RAND USR 16525
7150 RETURN
7550 RETURN
7600 FOR I=1 TO 20
7610 PRINT AT I,14;"{1}"
7620 NEXT I
7630 RETURN
7650 PRINT AT 12,4;"{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}{1}
{1}{1}{1}"
7660 GOTO 7600
7700 FOR I=3 TO 18 STEP 5
7710 PRINT AT I,5;"{1}{1}{1}{1}{1}{1}";AT I,21;"{1}{1}{1}{1}{1}{1}"
7720 NEXT I
7730 GOTO 7600
7750 FOR I=3 TO 19
7760 PRINT AT I,I;"{1}";AT I,32-I;"{1}"
7770 NEXT I
7780 RETURN
7800 FOR I=2 TO 20
7810 PRINT AT I,4;"{1}" AND I<16;AT I,9;"{1}" AND I>6;AT I,14;"{1}" AND I<16;AT I
,19;"{1}" AND I>6;AT I,24;"{1}" AND I<16;AT I,29;"{1}" AND I>6
7820 NEXT I
7830 RETURN
8000 LET A$="                      VOUS DIRIGEZ UN PION QUI SE DEPLACE SUR UNE SU
RFACE DELIMITEE PAR UN MUR. VOTRE BUT EST D""EVITER LES MURS AINSI QUE VOTRE PROP
RE TRACE JUSQU""A L""APPARITION DE LA SORTIE QUE VOUS DEVREZ ATTEINDRE POUR ACCED
ER AU TABLEAU SUIVANT...  UTILISEZ LES TOUCHES FLECHEES  ... PRESSEZ UNE TOUCHE."

8001 LET TABL=1
8002 LET VIE=3
8003 LET SCORE=0
8010 GOSUB 7000
8020 FOR I=1 TO LEN A$-31
8030 PRINT AT 0,0;A$(I TO I+31)
8040 NEXT I
8050 IF INKEY$="" THEN GOTO 8050
8060 RETURN