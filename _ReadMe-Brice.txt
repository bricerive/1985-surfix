################################################################
Moved Surfix and all I have about it under git
----------------------------------------------------------------
The tape recording WAV files are too big for git (as well as movies etc.)
Use Git Large File Storage extension.

Getting Started
Download and install the Git command line extension. Once downloaded and installed, set up Git LFS and its respective hooks by running:

git lfs install
You'll need to run this in your repository directory, once per repository.

Select the file types you'd like Git LFS to manage (or directly edit your .gitattributes). You can configure additional file extensions at anytime.

git lfs track "*.psd"
Make sure .gitattributes is tracked

git add .gitattributes
There is no step three. Just commit and push to GitHub as you normally would.

git add file.psd
git commit -m "Add design file"
git push origin master
----------------------------------------------------------------
